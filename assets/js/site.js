

$(window).load(function() {

    // ------------------------------------------------------------------------------------

    // Verifica se o plugin validationEngine() foi carregado
    if (jQuery().validationEngine) {
        $('.validar_form').validationEngine(); 
    } else {
    	console.log('Plugin validationEngine não foi carregado');
    }

    // ------------------------------------------------------------------------------------

    if (jQuery().mask) {
        $(".telefone").mask("(99) 9999-9999");
        $(".data").mask("99/99/9999");
        $(".cpf").mask("999.999.999-99");
        $(".cep").mask("99999-999");
    } else {
        console.log('Plugin mask não foi carregado');
    }

    // ------------------------------------------------------------------------------------

    // Remove o conteÃºdo de um modal ao fechar
    $('body').on('hidden', '.modal', function () {
        $(this).removeData('modal');
        $("#myModal .modal-body").html('Aguarde, carregando...');
    });

    // ------------------------------------------------------------------------------------

});



/*
	Chamando essa função faz uma animação rolando a
	tela até o topo da página
*/
function go_top() {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
}

