<?php

    switch ($page[0]) {

        case '404': 
            $title = 'A página acessada não pode ser encontrada | ' . _CONF_TITLE;
            $description = 'A página acessada não existe mais ou se encontra temporariamente desabilitada.';
            $keywords = _CONF_KEYWORDS;
            break;

        default : 
            $title = _CONF_TITLE;
            $description = _CONF_DESCRIPTION;
            $keywords = _CONF_KEYWORDS;
            break;

    }


    # Monta o header da página com title e description din�micos
    echo '<title>' . $title . '</title>';
    echo '<meta name="description" content="' . html_entity_decode($description) . '">';
    echo '<meta name="keywords" content="' . $keywords . '" />';
    echo '<meta name="author" content="' . _CONF_AUTHOR . '" />';
    echo '<meta name="language" content="pt-br" />';
    echo '<meta name="classification" content="' . _CONF_CLASSIFICATION . '">';
    echo '<meta property="og:locale" content="pt_BR"/>';
    echo '<meta property="og:type" content="article"/>';
    echo '<meta property="og:title" content="' . $title . '"/>';
    echo '<meta property="og:description" content="' . html_entity_decode($description) . '"/>';
    echo '<meta property="og:url" content="' . _HTTP . '"/>';
    echo '<meta property="og:site_name" content="' . $title . '"/>';
    echo '<script type="text/javascript">';
    echo "  var http = '" . _HTTP . "';";
    echo "  var root = '" . _ROOT . "';";
    echo '</script>';
          
?>