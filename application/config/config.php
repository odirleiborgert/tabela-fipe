<?php

    # Constantes
    require_once("const.php");

    # Autoloader
    require_once(_ROOT . 'application/class/Sonar/SplClassLoader.php');


    $classLoader = new Sonar_SplClassLoader(NULL, _ROOT . 'application/class');
    $classLoader->register();

    # Adiciona a um array a estrutura da página
    $page = isset($_GET['page']) ? explode('/', str_replace('.php', '', str_replace('.html', '', $_GET['page']))) : NULL;

    # A pagina que será feito o require ( posteriormente no index é feito: require($require) )
    # Só busca a página se caso $require não foi setada anteriormente
    if (empty($page[0])) {
        $require = _ROOT . _CONF_PAGE;
    } else {
        $require = (file_exists(_ROOT . $page[0] . '.php')) ? _ROOT . $page[0] . '.php' : _ROOT . _CONF_PAGE_ERROR;
    }

    # Importa a classe de conexão
    if (_CON) {
        $conn = new Sonar_Conexao();
        $con = $conn->connect();
        $con->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ); 
        global $con;        
    }

    # Importa as funções
    require_once(_ROOT . 'application/helpers/helper.php');

?>
