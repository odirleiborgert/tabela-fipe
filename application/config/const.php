<?php

    # Define configurações gerais
    define('_CONF_VERSION','0.2'); // Versão do Skeleton
    define('_CONF_ROOT','fipe'); // Pasta onde se encontra o projeto
    define('_CONF_PAGE','home.php'); // Primeira página que vai abrir
    define('_CONF_PAGE_ERROR','404.php'); // Página de erro
    define('_CONF_TITLE','Tabela FIPE'); // Título do site
    define('_CONF_DESCRIPTION',''); // Até 130 caracteres
    define('_CONF_KEYWORDS',''); // Até 20 palavras-chave
    DEFINE('_CONF_CLASSIFICATION',''); // Classificação do site, ex: Internet, Automóveis, Móveis
    define('_CONF_AUTHOR','agênciaSONAR'); // Autor do site
    define('_CONF_ANALYTICS',''); // Código Analytics
    define('_CONF_PRODUCTION',FALSE); // Modo de status do projeto

    # Define configuração para a função sendMAIL
    define('_MAIL_HOST',''); // SMTP 
    define('_MAIL_USER',''); // Usuário
    define('_MAIL_PASS',''); // Senha
    define('_MAIL_EMAIL',''); // E-mail
    define('_MAIL_NAME',''); // Título

    # Define conexão para o banco da Entidade
    define('_CON_HOST','localhost'); // Host do banco de dados (localhost)
    define('_CON_USER','root'); // Usuário do banco de dados
    define('_CON_PASS',''); // Senha do banco de dados
    define('_CON_DB','fipe'); // Nome do banco de dados 
    define('_CON',TRUE); // Definindo se terá banco de dados ou não

    # Caminho ROOT e HTTP
    define('_ROOT',str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . '/' . _CONF_ROOT . '/'));
    define('_HTTP','http://' . str_replace('//', '/', $_SERVER['HTTP_HOST'] . '/' . _CONF_ROOT . '/'));

?>
