<?php


	class Marcas {

		// -------------------------------------------------------------------------------------------
		
		public function __construct($tipo){

			global $con;
			$this->con = $con;

			$this->tipo = $tipo;

			switch ($tipo) {
				case 'carro':
					$this->tipo_id = 51;
					$this->tipo_extra = '';
					break;
				
				case 'moto':
					$this->tipo_id = 52;
					$this->tipo_extra = 'v=m&';
					break;
				
				case 'caminhao':
					$this->tipo_id = 53;
					$this->tipo_extra = 'v=c&';
					break;
				
			}

		}

		// -------------------------------------------------------------------------------------------

		public function findAll(){
			$sql = "SELECT *, (SELECT COUNT(*) FROM fipe_modelos WHERE fipe_marcas.id = fipe_modelos.id_marca) AS count_modelos FROM fipe_marcas WHERE tipo = :tipo ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':tipo', $this->tipo);
			$res->execute();
			$this->countFindAll = $res->rowCount(); 
			$this->findAll = $res->fetchAll();
		}

		// -------------------------------------------------------------------------------------------


		public function getMarcasFipe(){

			$inicial = file_get_contents("http://www.fipe.org.br/web/indices/veiculos/default.aspx?" . $this->tipo_extra . "p=" . $this->tipo_id);

			$vsMarca = pegaViewstate(explode("\r\n", $inicial));
			$evMarca = pegaEventValidation(explode("\r\n", $inicial));

			$marcas_extraido = extraiMarcas($inicial);

			return $marcas_extraido;

		}

		// -------------------------------------------------------------------------------------------

		public function importMarcasFipe(){

			$marcas = $this->getMarcasFipe();

			foreach($marcas as $key => $value) {

				// Insere ou faz Update
				if($this->checkMarca($value['codigo']) > 0){
					$this->updateMarca($value['nome'],$value['codigo']);
				} else {
					$this->insertMarca($value['nome'],$value['codigo']);
				}

			}

		}

		// -------------------------------------------------------------------------------------------

		private function checkMarca($id){
			
			$sql = "SELECT * FROM fipe_marcas WHERE id = :id ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':id', $id);
			$res->execute();
			return $res->rowCount();  

		}

		// -------------------------------------------------------------------------------------------

		private function insertMarca($marca,$id){
			$sql = "INSERT INTO fipe_marcas (id,marca,tipo,data_cadastro) VALUES (:id,:marca,:tipo,NOW())";
			$res = $this->con->prepare($sql);
			$res->bindParam(':marca', $marca, PDO::PARAM_STR);
			$res->bindParam(':tipo', $this->tipo, PDO::PARAM_STR);
			$res->bindParam(':id', $id, PDO::PARAM_INT);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------

		private function updateMarca($marca,$id){
			$sql = "UPDATE fipe_marcas SET marca = :marca, data_atualizacao = NOW() WHERE id = :id ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':marca', $marca, PDO::PARAM_STR);
			$res->bindParam(':id', $id, PDO::PARAM_INT);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------



	}



?>