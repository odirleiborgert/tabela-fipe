<?php


	class Modelos {

		// -------------------------------------------------------------------------------------------
		
		public function __construct($tipo){

			global $con;
			$this->con = $con;

			$this->tipo = $tipo;

			switch ($tipo) {
				case 'carro':
					$this->tipo_id = 51;
					$this->tipo_extra = '';
					break;
				
				case 'moto':
					$this->tipo_id = 52;
					$this->tipo_extra = 'v=m&';
					break;
				
				case 'caminhao':
					$this->tipo_id = 53;
					$this->tipo_extra = 'v=c&';
					break;
				
			}

		}

		// -------------------------------------------------------------------------------------------

		public function setIdMarca($marca){
			$this->id_marca = $marca;
		}

		// -------------------------------------------------------------------------------------------

		public function findAll(){
			$sql = "SELECT 
						*, 
						(SELECT COUNT(*) FROM fipe_modelos WHERE fipe_marcas.id = fipe_modelos.id_marca) AS count_modelos ,
						(SELECT data_atualizacao FROM fipe_relatorio WHERE fipe_marcas.id = fipe_relatorio.id_marca ORDER BY data_atualizacao DESC LIMIT 0,1) AS data_atualizacao

					FROM fipe_marcas WHERE tipo = :tipo ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':tipo', $this->tipo);
			$res->execute();
			$this->countFindAll = $res->rowCount(); 
			$this->findAll = $res->fetchAll();
		}

		// -------------------------------------------------------------------------------------------

		public function countAnoModelo($id_marca){

			// Pegando os modelos de uma marca
			$sql = "SELECT id FROM fipe_modelos WHERE id_marca = :id_marca ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':id_marca', $id_marca);
			$res->execute();
			$modelos = $res->fetchAll();

			$count = 0;
			foreach ($modelos as $key => $value) {
				$sql = "SELECT COUNT(*) AS total FROM fipe_ano_modelo WHERE id_modelo = :id_modelo ";
				$res = $this->con->prepare($sql);
				$res->bindParam(':id_modelo', $value->id, PDO::PARAM_STR);
				$res->execute(); 
				$num = $res->fetch();
				$count += $num->total;
			}

			return $count; 
		}

		// -------------------------------------------------------------------------------------------

		public function getModelosFipe(){

			if(!empty($this->id_marca)){

				$this->runViewStateEventValidationMarca();

				$modelo_post = _get("http", "fipe.org.br", "80", "/web/indices/veiculos/default.aspx?" . $this->tipo_extra . "p=" . $this->tipo_id , array("ScriptManager1"=>"ScriptManager1|ddlMarca", "__EVENTTARGET"=>"ddlMarca", "__EVENTVALIDATION"=>$this->evMarca, "__VIEWSTATE"=>$this->vsMarca, "ddlMarca"=>$this->id_marca, "ddlAnoValor"=>0, "ddlModelo"=>0));

				$this->vsModelo = pegaViewstate(explode("\r\n", $modelo_post));
				$this->evModelo = pegaEventValidation(explode("\r\n", $modelo_post));

				$modelos_extraido = extraiModelos($modelo_post);

				return $modelos_extraido;

			}

		}

		// -------------------------------------------------------------------------------------------

		public function importModelosFipe(){

			$modelos = $this->getModelosFipe();

			foreach($modelos as $key => $value) {

				// Insere ou faz Update
				if($this->checkModelo($value['codigo']) > 0){
					$this->updateModelo($value['nome'],$value['codigo']);
				} else {
					$this->insertModelo($value['nome'],$value['codigo']);
				}

				// ---------------------

				// Importar AnoModelo

				$ano_modelo_post = _get("http", "fipe.org.br", "80", "/web/indices/veiculos/default.aspx?" . $this->tipo_extra . "p=" . $this->tipo_id , array("ScriptManager1"=>"updModelo|ddlModelo", "__EVENTTARGET"=>"ddlModelo", "__EVENTVALIDATION" => $this->evModelo, "__VIEWSTATE" => $this->vsModelo, "ddlMarca"=>$this->id_marca, "ddlAnoValor"=>0, "ddlModelo"=>$value['codigo']));

				$this->vsAnoModelo = pegaViewstate(explode("\r\n", $ano_modelo_post));
				$this->evAnoModelo = pegaEventValidation(explode("\r\n", $ano_modelo_post));

				$ano_modelos_extraido = extraiAnoModelos($ano_modelo_post);

				foreach($ano_modelos_extraido as $ano_modelo) {
					
					$valor_post = _get("http", "fipe.org.br", "80", "/web/indices/veiculos/default.aspx?" . $this->tipo_extra . "p=" . $this->tipo_id , array("ScriptManager1"=>"updAnoValor|ddlAnoValor", "__EVENTTARGET"=>"ddlAnoValor", "__EVENTVALIDATION"=>$this->evAnoModelo, "__VIEWSTATE"=>$this->vsAnoModelo, "ddlMarca"=>$this->id_marca, "ddlAnoValor"=>$ano_modelo['codigo'], "ddlModelo"=>$value['codigo']));

					$valor = extraiValor($valor_post);

					// Insere ou faz Update
					if($this->checkAnoModelo($ano_modelo['codigo']) > 0){
						$this->updateAnoModelo($ano_modelo['codigo'],$ano_modelo['nome'],$valor);
					} else {
						$this->insertAnoModelo($ano_modelo['codigo'],$ano_modelo['nome'],$valor,$value['codigo']);
					}

				}

				// ---------------------
	

			}

			// Gravando o relatório

			$this->setRelatorio();

			// ------------------------

		}

		// -------------------------------------------------------------------------------------------

		private function checkModelo($id){
			
			$sql = "SELECT * FROM fipe_modelos WHERE id = :id ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':id', $id);
			$res->execute();
			return $res->rowCount();  

		}

		// -------------------------------------------------------------------------------------------

		private function insertModelo($modelo,$id){
			$sql = "INSERT INTO fipe_modelos (id,modelo,id_marca,tipo,data_cadastro) VALUES (:id,:modelo,:id_marca,:tipo,NOW())";
			$res = $this->con->prepare($sql);
			$res->bindParam(':modelo', $modelo, PDO::PARAM_STR);
			$res->bindParam(':tipo', $this->tipo, PDO::PARAM_STR);
			$res->bindParam(':id', $id, PDO::PARAM_STR);
			$res->bindParam(':id_marca', $this->id_marca, PDO::PARAM_INT);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------

		private function updateModelo($modelo,$id){
			$sql = "UPDATE fipe_modelos SET modelo = :modelo, data_atualizacao = NOW() WHERE id = :id ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':modelo', $modelo, PDO::PARAM_STR);
			$res->bindParam(':id', $id, PDO::PARAM_STR);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------

		private function runViewStateEventValidationMarca(){

			$inicial = file_get_contents("http://www.fipe.org.br/web/indices/veiculos/default.aspx?" . $this->tipo_extra . "p=" . $this->tipo_id);

			$this->vsMarca = pegaViewstate(explode("\r\n", $inicial));
			$this->evMarca = pegaEventValidation(explode("\r\n", $inicial));

		}

		// -------------------------------------------------------------------------------------------

		private function checkAnoModelo($id){
			
			$sql = "SELECT * FROM fipe_ano_modelo WHERE id = :id ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':id', $id);
			$res->execute();
			return $res->rowCount();  

		}

		// -------------------------------------------------------------------------------------------

		private function insertAnoModelo($id,$ano_modelo,$valor,$id_modelo){
			$sql = "INSERT INTO fipe_ano_modelo (id,ano_modelo,id_modelo,valor,tipo,data_cadastro) VALUES (:id,:ano_modelo,:id_modelo,:valor,:tipo,NOW())";
			$res = $this->con->prepare($sql);
			$res->bindParam(':id', $id, PDO::PARAM_STR);
			$res->bindParam(':ano_modelo', $ano_modelo, PDO::PARAM_STR);
			$res->bindParam(':id_modelo', $id_modelo, PDO::PARAM_INT);
			$res->bindParam(':valor', $valor, PDO::PARAM_STR);
			$res->bindParam(':tipo', $this->tipo, PDO::PARAM_STR);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------

		private function updateAnoModelo($id,$ano_modelo,$valor){
			$sql = "UPDATE fipe_ano_modelo SET ano_modelo = :ano_modelo, valor = :valor, data_atualizacao = NOW() WHERE id = :id ";
			$res = $this->con->prepare($sql);
			$res->bindParam(':ano_modelo', $ano_modelo, PDO::PARAM_STR);
			$res->bindParam(':valor', $valor, PDO::PARAM_STR);
			$res->bindParam(':id', $id, PDO::PARAM_STR);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------

		private function setRelatorio(){
			$sql = "INSERT INTO fipe_relatorio (id_marca,tipo,data_atualizacao) VALUES (:id_marca,:tipo,NOW())";
			$res = $this->con->prepare($sql);
			$res->bindParam(':id_marca', $this->id_marca, PDO::PARAM_INT);
			$res->bindParam(':tipo', $this->tipo, PDO::PARAM_STR);
			$res->execute();
		}

		// -------------------------------------------------------------------------------------------


	}



?>