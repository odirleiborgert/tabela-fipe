<?php

    class Sonar_Conexao {
        
        private $host;
        private $user;
        private $pass;
        private $db;
        private $dsn;

        // -----------------------------------------------------------------------------------------------------

        /**
         * Construtor da classe
         */
        public function __construct(){

            $this->host     = _CON_HOST;
            $this->user     = _CON_USER;
            $this->pass     = _CON_PASS;
            $this->db       = _CON_DB;
            $this->dsn      = "mysql:host=" . $this->host . ";dbname=" . $this->db . ";";
        }

        // -----------------------------------------------------------------------------------------------------

        /**
         * Método para conectar ao banco de dados
         * @return \PDO
         */
        public function connect(){
            
            try {
                $connection = new PDO($this->dsn,$this->user,$this->pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                 $connection->exec("set names utf8");
                return $connection;
            } catch (PDOException $e) {
                require("application/offline/em-manutencao.php");
                exit;
            }
            
        }

        // -----------------------------------------------------------------------------------------------------

    }

?>
