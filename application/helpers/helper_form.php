<?php
    
    // --------------------------------------------------------------------------------------------------------

    /**
     * marca_campo() - Marca resultados em checkbox, radiogroup e select
     * @param type $p1
     * @param type $p2
     * @param type $opt
     * @return string
     */
    function marca_campo($p1, $p2, $opt) {
        if ($p1 == $p2)
            switch ($opt) {
                case 'check':
                case 'radio':
                    return 'checked="checked"';
                    break;
                case 'select':
                    return 'selected="selected"';
            }
    }

    // --------------------------------------------------------------------------------------------------------
    
?>