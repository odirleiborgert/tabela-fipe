<?php
    
    // --------------------------------------------------------------------------------------------------------
    
    /**
     * format_money() - Converta para formato dinheiro
     * @param type $value
     * @return type
     */
    function format_money($value) {
        if (is_numeric($value)) {
            return number_format($value, 2, ',', '.');
        } else {
            return str_replace(',', '.', str_replace('.', '', $value));
        }
    }

    // --------------------------------------------------------------------------------------------------------
    
?>