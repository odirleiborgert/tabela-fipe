<?php
    
    // --------------------------------------------------------------------------------------------------------

    /**
     * date_transform() - Formata a data, se for EN transforma em PT e vice-versa
     * @param type $date
     * @param type $time
     * @return string
     */
    function date_transform($date, $time = false) {
        if (!empty($date)) {
            if (strstr($date, '-')) {
                if (strstr($date, ':')) {
                    list($year, $month, $day) = explode('-', $date);
                    list($day, $hours) = explode(' ', $day);
                    list($hour, $minute, $seconds) = explode(':', $hours);
                    if ($time) {
                        $date = $day . '/' . $month . '/' . $year . ' às ' . $hour . 'h' . $minute . 'min.';
                    } else {
                        $date = $day . '/' . $month . '/' . $year;
                    }
                } else {
                    list($year, $month, $day) = explode('-', $date);
                    $date = $day . '/' . $month . '/' . $year;
                }
            } else {
                if (strstr($date, ' ')) {
                    list($full_date, $full_hour) = explode(' ', $date);
                    list($day, $month, $year) = explode('/', $full_date);
                    list($hour, $minute, $second) = explode(':', $full_hour);

                    $date = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':' . $second;
                } else {
                    list($day, $month, $year) = explode('/', $date);
                    $date = $year . '-' . $month . '-' . $day;
                }
            }

            return $date;
        }
    }
    
    // --------------------------------------------------------------------------------------------------------

    function set_data_extenso($strDate) {
        
        // Array com os meses do ano em português;
        $arrMonthsOfYear = array(1 => 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        
        // Descobre o dia do mês
        $intDayOfMonth = date('d', strtotime($strDate));
        
        // Descobre o mês
        $intMonthOfYear = date('n', strtotime($strDate));
        
        // Descobre o ano
        $intYear = date('Y', strtotime($strDate));
        
        // Formato a ser retornado
        return $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;
        
    }

    // --------------------------------------------------------------------------------------------------------

    function formata_data_extenso($strDate) {
        
        list($data, $hora) = explode(' ', $strDate);
     
        list($ano, $mes, $dia) = explode('-', $data);
        list ($horas, $minutos, $segundos) = explode(':', $hora);
     
        return "$dia de " . mes_extenso($mes) . " de " . $ano . " às {$horas}h{$minutos}min";

    }
    
    // --------------------------------------------------------------------------------------------------------

    /**
     * mes_extenso() - Retorna o mês por extenso
     * @param type $mes
     * @param type $abrv
     * @return string|boolean
     */
    function mes_extenso($mes, $abrv = FALSE) {
        
        if($abrv==TRUE){
            switch($mes){
                case 01: return 'JAN'; break;
                case 02: return 'FEV'; break;
                case 03: return 'MAR'; break;
                case 04: return 'ABR'; break;
                case 05: return 'MAI'; break;
                case 06: return 'JUN'; break;
                case 07: return 'JUL'; break;
                case 08: return 'AGO'; break;
                case 09: return 'SET'; break;
                case 10: return 'OUT'; break;
                case 11: return 'NOV'; break;
                case 12: return 'DEZ'; break;
            }
        }     
        
        $mes--;
        $array = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
        if(isset($array[$mes])){
            return $array[$mes];
        }
        return false;
        
    }

    // --------------------------------------------------------------------------------------------------------
    
?>