<?php
    
    // --------------------------------------------------------------------------------------------------------

    /**
     * url_title() - Monta URL para links
     * @param type $title
     * @param type $html
     * @return type
     */
    function url_title($title, $html = false) {
        $words = explode(' ', strtolower(remove_special_char(trim($title))));
        $url = '';
        $total = 0;
        foreach ($words as $value) {
            ++$total;
            $url .= $value . '-';
        }
        $url = substr($url, 0, strlen($url) - 1);
        if ($html)
            $url .= '.html';
        $url = str_replace(',', '', str_replace('--', '-', $url));
        return $url;
    }
    
    // --------------------------------------------------------------------------------------------------------

    /**
     * remove_special_char() - Função para remover caracteres especiais
     * @param type $string
     * @return type
     */
    function remove_special_char($string) {
        
        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ"!@#$%&*()_-+={[}]/?;:,\\\'<>';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                             ';
        $string = utf8_decode($string);
        $string = strtr($string, utf8_decode($a), $b);
        $string = strip_tags(trim($string));

        /*Agora, remove qualquer espaço em branco duplicado*/
        $string = preg_replace('/\s(?=\s)/', '', $string);

        /*Ssubstitui qualquer espaço em branco (não-espaço), com um espaço*/
        $string = preg_replace('/[\n\r\t]/', ' ', $string);

        /*Remove qualquer espaço vazio*/
        $string = str_replace(" ","-",$string);

        return strtolower(utf8_encode($string));
        
    }
    
    // --------------------------------------------------------------------------------------------------------

    /**
     * char_limit() - Limita a quantidade de caracteres
     * @param type $text
     * @param type $limit
     * @param type $strip
     * @return string
     */
    function char_limit($text, $limit = 300, $strip = false) {
        if ($strip)
            $text = strip_tags($text);
        $words = explode(' ', $text);
        $monted = '';
        foreach ($words as $value) {
            if (strlen($monted) < $limit) {
                $monted .= ' ' . $value;
            } else {
                if ((strstr(',', substr($monted, -1))) OR (strstr('-', substr($monted, -1))) OR (strstr('!', substr($monted, -1))) OR (strstr('?', substr($monted, -1))) OR (strstr('.', substr($monted, -1))))
                    $monted = substr($monted, 0, strlen($monted) - 1);
                $monted .= '...';
                return $monted;
            }
        }
        return $monted;
    }

    // --------------------------------------------------------------------------------------------------------
    
?>