<?php
    
    // --------------------------------------------------------------------------------------------------------

    include('helper_date.php');     // Trabalhando com data 
    include('helper_float.php');    // Trabalhando com valores
    include('helper_form.php');     // Trabalhando com formulários 
    include('helper_string.php');   // Trabalhando com string
    include('helper_fipe.php');   // Trabalhando com funções para tabela fipe

    // --------------------------------------------------------------------------------------------------------
    
?>