<div class="home">
    <h2>Estrutura da tabela</h2>
    

    <pre class="prettyprint">
	CREATE TABLE IF NOT EXISTS `fipe_ano_modelo` (
	  `id` varchar(100) CHARACTER SET latin1 NOT NULL,
	  `id_modelo` int(11) NOT NULL,
	  `ano_modelo` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `valor` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `tipo` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `data_cadastro` datetime NOT NULL,
	  `data_atualizacao` datetime NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE IF NOT EXISTS `fipe_marcas` (
	  `id` int(11) NOT NULL,
	  `marca` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `tipo` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `data_cadastro` datetime NOT NULL,
	  `data_atualizacao` datetime NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	 
	CREATE TABLE IF NOT EXISTS `fipe_modelos` (
	  `id` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `id_marca` int(11) NOT NULL,
	  `modelo` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `tipo` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `data_cadastro` datetime NOT NULL,
	  `data_atualizacao` datetime NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE IF NOT EXISTS `fipe_relatorio` (
	  `id_marca` int(11) NOT NULL,
	  `tipo` varchar(255) CHARACTER SET latin1 NOT NULL,
	  `data_atualizacao` datetime NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</pre>
</div>