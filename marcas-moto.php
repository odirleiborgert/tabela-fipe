<div class="marcas">
    
    <h1>Marcas <small>Moto (Total de <?php echo $marcas->countFindAll ?> em minha base de dados)</small></h1>

	<ul class="nav nav-tabs">
		<li><a href="<?php echo _HTTP; ?>marcas/carro.html">Carro</a></li>
		<li class="active"><a href="<?php echo _HTTP; ?>marcas/moto.html">Moto</a></li>
		<li><a href="<?php echo _HTTP; ?>marcas/caminhao.html">Caminhão</a></li>
	</ul>

	<form action="" method="post">
		<input type="hidden" name="acao" value="importMarcas">
		<button type="submit" class="btn btn-primary btn-large">Importar / Atualizar Marcas</button>
		<a href="<?php echo _HTTP; ?>modal/marcas-moto.php" role="button" class="btn btn-large" data-target="#myModal" data-toggle="modal">Contar registros no site da FIPE</a>
	</form>

	<hr />

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Código</th>
				<th>Marca</th>
				<th>Tipo</th>
				<th>Modelos</th>
				<th>Última Cadastro</th>
				<th>Última Atualização</th>
			</tr>
		</thead>
		<tbody>
            <?php foreach($marcas->findAll as $key => $value){ ?>
                <tr>
                    <td><?php echo $value->id; ?></td>
                    <td><?php echo $value->marca; ?></td>
                    <td><?php echo $value->tipo; ?></td>
                    <td><?php echo $value->count_modelos; ?></td>
                    <td><?php echo date_transform($value->data_cadastro,true) ?></td>
                    <td><?php echo date_transform($value->data_atualizacao,true) ?></td>
                </tr>
            <?php } ?>
        </tbody>
	</table>
    
</div>


<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Registros no site da fipe</h3>
	</div>
	<div class="modal-body">
		Aguarde, carregando...
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
	</div>
</div>