<?php 
    
    // Inicia sessão
    session_start();

    // Mostrando os ERROS do PHP
    error_reporting(E_ALL); 
    ini_set("display_errors", 1); 


    require("application/config/config.php"); 

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width">

        <?php require('application/config/header.php'); ?>

        <!-- CSS e Google Web Fonts -->
        <link rel="stylesheet" href="<?php echo _HTTP ?>assets/plugins/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo _HTTP ?>assets/css/site.css">

        <!-- CSS de PLugins -->
        <link rel="stylesheet" href="<?php echo _HTTP ?>assets/plugins/jquery.validationEngine/validationEngine.jquery.min.css">

    </head>
    <body>


        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="<?php echo _HTTP; ?>">Tabela Fipe</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="<?php echo _HTTP; ?>marcas.html">Marcas</a></li>
                            <li><a href="<?php echo _HTTP; ?>modelos.html">Modelos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

        <div class="container">
            
            <?php require($require); ?>            
            
        </div>

        <div class="footer">
            <div class="container">
                <p>Sistema de importação da tabela fipe.</p>
            </div>  
        </div>




        <!-- JavaScript -->
        <script src="<?php echo _HTTP ?>assets/js/vendor/jquery-1.9.1.min.js"></script>
        <script src="<?php echo _HTTP ?>assets/plugins/jquery.validationEngine/languages/jquery.validationEngine-pt_BR.js"></script>
        <script src="<?php echo _HTTP ?>assets/plugins/jquery.validationEngine/jquery.validationEngine.min.js"></script>
        <script src="<?php echo _HTTP ?>assets/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="<?php echo _HTTP ?>assets/js/plugins.js"></script>
        <script src="<?php echo _HTTP ?>assets/js/site.js"></script>

    </body>
</html>
