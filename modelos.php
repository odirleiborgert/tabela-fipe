<?php

	// -------------------------------------------------------------------------------------------

	// Escolhendo o tipo de filtro
	if(isset($page[1])){
		$tipo = $page[1];
	} else {
		$tipo = 'carro';
	}

	// -------------------------------------------------------------------------------------------

	$modelos = new Modelos($tipo);

	// -------------------------------------------------------------------------------------------	

	// Faz a importação das modelos
	if(isset($_POST['acao']) AND $_POST['acao']=='importModelos'){
		$modelos->setIdMarca(@$_POST['id_marca']);
		$modelos->importModelosFipe();
	}

	// -------------------------------------------------------------------------------------------

	// Pega as modelos de determinado tipo
	$modelos->findAll();

	// -------------------------------------------------------------------------------------------

	// CHamando a pagina para carregar
    if(isset($page[1])){
        require("modelos-" . $page[1] . ".php");
    } else {
        require("modelos-carro.php");
    }

    // -------------------------------------------------------------------------------------------

?>