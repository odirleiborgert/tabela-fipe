<div class="modelos">
    
    <h1>Modelos <small>Carro</small></h1>

    <ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo _HTTP; ?>modelos/carro.html">Carro</a></li>
        <li><a href="<?php echo _HTTP; ?>modelos/moto.html">Moto</a></li>
        <li><a href="<?php echo _HTTP; ?>modelos/caminhao.html">Caminhão</a></li>
    </ul>

    <hr />

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Código</th>
                <th>Marca</th>
                <th>Tipo</th>
                <th>Modelos</th>
                <th>Ano Modelo</th>
                <th>Última Atualização</th>
                <th>Importar / Atualizar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($modelos->findAll as $key => $value){ ?>
                <tr>
                    <td><?php echo $value->id; ?></td>
                    <td><?php echo $value->marca; ?></td>
                    <td><?php echo $value->tipo; ?></td>
                    <td><?php echo $value->count_modelos; ?> <a href="<?php echo _HTTP; ?>modal/modelos-carro.php?id_marca=<?php echo $value->id; ?>&marca=<?php echo $value->marca; ?>" role="button" class="btn btn-mini" data-target="#myModal" data-toggle="modal">Check FIPE</a></td>
                    <td><?php echo $modelos->countAnoModelo($value->id); ?></td>
                    <td><?php echo date_transform($value->data_atualizacao,true); ?></td>
                    <td>
                        <form action="" method="post">
                            <input type="hidden" name="acao" value="importModelos">
                            <input type="hidden" name="id_marca" value="<?php echo $value->id; ?>">
                            <button type="submit" class="btn btn-primary">Atualizar Modelos</button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    
</div>


<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Registros no site da fipe</h3>
    </div>
    <div class="modal-body">
        Aguarde, carregando...
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
    </div>
</div>