<?php 

	// Inicia sessão
    session_start();

    // Mostrando os ERROS do PHP
    error_reporting(E_ALL); 
    ini_set("display_errors", 1); 

	require("../application/config/config.php"); 

	$tipo = 'carro';
	$marca = $_GET['marca'];
	$id_marca = $_GET['id_marca'];

	$modelos = new Modelos($tipo); 
	$modelos->setIdMarca($id_marca);
	$count = count($modelos->getModelosFipe());


?>
<h2 class="text-info"><?php echo $count; ?> modelos para <?php echo $marca; ?></h2>