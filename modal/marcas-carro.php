<?php 

	// Inicia sessão
    session_start();

    // Mostrando os ERROS do PHP
    error_reporting(E_ALL); 
    ini_set("display_errors", 1); 

	require("../application/config/config.php"); 

	$tipo = 'carro';

	$marcas = new Marcas($tipo); 
	$count = count($marcas->getMarcasFipe());


?>
<h2 class="text-info"><?php echo $count; ?> marcas de <?php echo $tipo; ?></h2>